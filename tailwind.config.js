module.exports = {
  purge: ['./public/index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      margin: {
        '-96': '-18rem'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
