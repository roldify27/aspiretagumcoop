export default function () {
  return {
    isLoggedIn: false,
    currentUser: null,
    apiEndpoint: 'http://192.168.23.197:3000/', // 'http://ec2-54-169-218-126.ap-southeast-1.compute.amazonaws.com:3000/',
    userInteractions: {
      isRegionSelected: false,
      isProvinceSelected: false,
      isCitySelected: false,
      isBarangaySelected: false,
      isSidebarOpen: false
    }
  }
}
