
export function toggleRegionSelection (state, payload) {
  state.userInteractions.isRegionSelected = payload
}

export function toggleShowSideBar (state, payload) {
  state.userInteractions.isSidebarOpen = !state.userInteractions.isSidebarOpen
}

export function setCurrentUser (state, payload) {
  state.currentUser = payload
}

export function unsetUser (state) {
  localStorage.clear()
  state.currentUser = null
}

export function setNewRegStatus (state, payload) {
  state.currentUser.RegistrationStatus = payload
}

export function resetAttachments (state, payload) {
  state.currentUser._ORProfileAttachments = payload
}
