import axios from 'axios'

export function toggleRegionSelection ({ commit, state }, payload) {
  commit('toggleRegionSelection', payload)
  return true
}

export function toggleShowSideBar ({ commit, state }, payload) {
  commit('toggleShowSideBar', payload)
  return true
}

export async function processLoginAdmin ({ commit, state }, payload) {
  let ls = payload
  if (ls !== null) {
    ls = ls.split('.')
    ls = JSON.parse(atob(ls[1]))
  }

  if (ls !== null) {
    commit('setCurrentUser', ls)
  }
}

export async function processLogin ({ commit, state }, payload) {
  let ls = payload
  if (ls !== null) {
    ls = ls.split('.')
    ls = JSON.parse(atob(ls[1]))
  }

  if (ls !== null) {
    console.log(state.apiEndpoint)
    await axios.get(state.apiEndpoint + 'membership-registration/v1/get-profile-for-update', {
      params: {
        profileId: ls.Id
      },
      headers: {
        'Authorization': localStorage.getItem('accessToken')
      }
    }).then(result => {
      console.log('im here')
      commit('setCurrentUser', result.data)
      return {
        status: true,
        msg: ''
      }
    }).catch(err => {
      console.log(err)
      commit('unsetUser')
    })
    // commit('setCurrentUser', ls)
  } else {
    return {
      status: false,
      msg: 'Invalid Credentials'
    }
  }
}

export async function unsetUser ({ commit, state }) {
  await axios.post(state.apiEndpoint + 'users/logout', {}, {
    headers: {
      'Authorization': localStorage.getItem('accessToken')
    }
  }).then(result => {
    if (result.data.status) {
      commit('unsetUser')
      return true
    }
  })
}

export async function resetAttachments ({ commit, state }, payload) {
  commit('resetAttachments', payload)
}

export async function setNewRegStatus ({ commit, state }, payload) {
  commit('setNewRegStatus', payload)
}
