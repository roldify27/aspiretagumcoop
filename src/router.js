import Vue from 'vue'
import Router from 'vue-router'
import MainLayout from './layouts/MainLayout.vue'
import Home from './views/Home.vue'
import Registration from './views/Registration.vue'
import PaymentMethods from './views/PaymentMethods.vue'
import RegistrantDashboard from './views/registrant/RegistrantDashboard.vue'
import Admin from './views/admin/AdminDashboard.vue'
import RegDashboard from './components/registrants/Dashboard.vue'
import ProfileUpdate from './components/registrants/ProfileUpdate.vue'
import AttachmentsUpdate from './components/registrants/AttachmentsUpdate.vue'
import AdminDashboard from './components/admin/Dashboard.vue'
import LoginLayout from './layouts/LoginLayout.vue'
import AdminLoginLayout from './layouts/AdminLoginLayout.vue'
import { ToastProgrammatic as Toast } from 'buefy'
import store from './store'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: MainLayout,
      children: [
        {
          path: '',
          component: Home,
          name: 'home'
        },
        {
          path: 'registration',
          component: Registration,
          name: 'registration'
        },
        {
          path: 'registrant-dashboard',
          component: RegistrantDashboard,
          children: [
            {
              path: '',
              component: RegDashboard,
              name: 'regDashboard'
            },
            {
              path: 'profile-update',
              component: ProfileUpdate,
              name: 'profile-update'
            },
            {
              path: 'attachments-update',
              component: AttachmentsUpdate,
              name: 'attachments-update'
            },
            {
              path: 'paymentmethods',
              component: PaymentMethods,
              name: 'paymentmethods'
            }
          ]
        },
        {
          path: 'about',
          name: 'about',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: function () {
            return import(/* webpackChunkName: "about" */ './views/About.vue')
          }
        }
      ]
    },
    {
      path: '/login',
      component: LoginLayout
    },
    {
      path: '/login-admin-xyz',
      component: AdminLoginLayout
    },
    {
      path: '/admin',
      component: MainLayout,
      children: [
        {
          path: '',
          name: 'admin',
          component: Admin,
          children: [
            {
              path: '',
              name: 'admindashboard',
              component: AdminDashboard
            }
          ]
        }
      ]
    }
  ]

})

router.beforeEach(async (to, from, next) => {
  let isValid = await validateRoute(to)
  console.log(isValid)
  if (isValid.status) {
    next()
  } else {
    if (isValid.message !== '') {
      Toast.open({
        duration: 3000,
        message: isValid.message,
        type: 'is-danger'
      })
    }

    if (to.path !== '/login') {
      next('/login')
    } else {
      next(from.path)
    }
  }
})

/**
 *
 * @param {*} path - route name/path to go
 * @returns boolean true if route is valid, false, if route is invalid.
 * Ex: VISITOR MUST NOT be able to access dashboard UI because he isnt registered yet.
 */
async function validateRoute (path) {
  path = typeof (path.name) === 'undefined' ? path.path : path.name

  var user = store.getters['visitor/getCurrentUser'] // check if user is in vuex store
  if (user === null) {
    var tokenPieces = localStorage.getItem('accessToken') // check if access token is still available
    if (tokenPieces !== null) { // if available, populate user in vuex store
      if (path === 'admindashboard' || path === '/admindashboard') {
        await store.dispatch('visitor/processLoginAdmin', tokenPieces)
      } else {
        await store.dispatch('visitor/processLogin', tokenPieces)
      }
      user = store.getters['visitor/getCurrentUser']
    }
  }

  if (path === 'regDashboard' || path === '/registrant-dashboard') {
    if (user === null) {
      return {
        status: false,
        message: 'Unauthorized Access'
      }
    } else {
      if (user.RegistrationStatus === 'MSSVERIFICATION' || user.RegistrationStatus === 'MSSPENDING' || user.RegistrationStatus === 'PAYMENTPENDING' || user.RegistrationStatus === 'MSSPAYMENTVERIFIED' || user.RegistrationStatus === 'MSSDENIED' || user.RegistrationStatus === 'ACTGPAYMENTVERIFIED') {
        return {
          status: true,
          message: 's'
        }
      } else {
        return {
          status: false,
          message: 'Unauthorized Access'
        }
      }
    }
  } else if (path === '/login' || path === 'login' || path === '/login-admin-xyz' || path === 'login-admin-xyz') {
    console.log('localstorage', localStorage.getItem('accessToken'))
    if (user === null || localStorage.getItem('accessToken') === null) {
      return { status: true, message: '' }
    } else {
      return { status: false, message: '' }
    }
  } else if (path === 'profile-update' && user.RegistrationStatus !== 'MSSDENIED') {
    return {
      status: false,
      message: 'Unauthorized Access'
    }
  } else {
    return {
      status: true,
      message: ''
    }
  }
}

export default router
