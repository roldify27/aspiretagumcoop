
export default {
  computed: {

  },

  methods: {
    setErrorMessage (attr) {
      if (!attr.$error) { return }

      if (typeof (attr.required) !== 'undefined' && !attr.required) { return 'Ooops! This field is required' }
      if (typeof (attr.minLength) !== 'undefined' && !attr.minLength) { return 'Ooops! Minimum length is ' + attr.$params.minLength.min }
      if (typeof (attr.alpha) !== 'undefined' && !attr.alpha) { return 'Ooops! No numbers/special characters for this field' }
      if (typeof (attr.numeric) !== 'undefined' && !attr.numeric) { return 'Ooops! Only numbers are allowed in this field' }
      if (typeof (attr.email) !== 'undefined' && !attr.email) { return 'Ooops! Email format is invalid' }
      if (typeof (attr.sameAsPassword) !== 'undefined' && !attr.sameAsPassword) { return 'Ooops! Password Mismatch' }
      if (typeof (attr.isFilipino) !== 'undefined' && !attr.isFilipino) { return 'Ooops! For the meantime this service is open to Filipinos only' }
      if (typeof (attr.mustBeEqualToPO) !== 'undefined' && !attr.mustBeEqualToPO) { return 'Ooops! Invalid Amount to be paid. Kindly refer to Payment Order.' }
      if (typeof (attr.maxValue) !== 'undefined' && !attr.maxValue) { return 'Ooops! This date should not be later than today.' }
    },

    getPlaceLabel (placeObject) {
      // let location = vm.isFromPh ?  {
      //     isFromPh: vm.isFromPh,
      //     region: vm.selected,
      //     province: vm.provinceSelected,
      //     city: vm.citySelected,
      //     barangay: vm.includeBarangay ? vm.barangaySelected : {},
      //     hasBarangay: vm.includeBarangay
      // } : {
      //   isFromPh: vm.isFromPh,
      //   country: vm.country,
      //   overseas_location: vm.overseas_location
      // };

      if (placeObject === null || typeof (placeObject) === 'undefined') {
        return ''
      }

      let brgyLabel = typeof (placeObject.barangay) === 'undefined' ? '' : placeObject.barangay.label

      if (placeObject.isFromPh) {
        if (typeof (placeObject.barangay) === 'undefined') {
          return ''
        }
        return brgyLabel + ', ' + placeObject.city.label + ', ' + placeObject.province.label + ' ' + placeObject.region.label
      } else {
        if (typeof (placeObject.overseas_location) === 'undefined') { return '' }

        return placeObject.overseas_location + ', ' + placeObject.country.label
      }
    }
  }

}
