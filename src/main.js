import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Buefy from 'buefy'
import './styles/vegeta.scss'
import localComponents from './local_components'
import './styles/stylemo.css'
import axios from 'axios'
import { library } from '@fortawesome/fontawesome-svg-core'
import 'material-icons/iconfont/material-icons.css'
import VModal from 'vue-js-modal'

// internal icons
import { faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
  faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faUser,
  faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload, faHospitalUser, faDownload, faForward, faBackward, faPlus, faSpinner, faCalendar, faGlobeAmericas, faPhoneSquareAlt, faMobileAlt, faEnvelopeSquare, faKey, faUserLock, faRedoAlt, faTrashAlt, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { VueMaskDirective } from 'v-mask'
import Vuelidate from 'vuelidate'
// import API from './api/index'
Vue.use(Vuelidate)

Vue.directive('mask', VueMaskDirective)
library.add(faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
  faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faUser,
  faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload, faHospitalUser, faDownload, faForward, faBackward, faPlus, faSpinner, faCalendar, faGlobeAmericas, faPhoneSquareAlt, faMobileAlt, faEnvelopeSquare, faKey, faUserLock, faRedoAlt, faTrashAlt, faTimes)
Vue.component('vue-fontawesome', FontAwesomeIcon)
Vue.use(VModal, {})
Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas'
})

// Vue.prototype.$API = API
Vue.prototype.$axios = axios
Vue.config.productionTip = false

new Vue({
  router,
  store,
  components: {
    localComponents
  },
  render: function (h) { return h(App) }
}).$mount('#app')
