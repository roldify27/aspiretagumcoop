
export default {
  filters: {
    // we want to return 05/31/2021 2:31PM
    filterDateWithTime: function (value) {
      let date = new Date(value)
      let monthPart = date.getMonth() + 1
      let datePart = date.getDate()
      let yearPart = date.getFullYear()
      let hourPart = date.getHours()
      let minutePart = date.getMinutes()
      let ampm = hourPart >= 12 ? 'pm' : 'am'
      hourPart = hourPart % 12
      hourPart = hourPart || 12
      minutePart = minutePart < 10 ? '0' + minutePart : minutePart
      monthPart = monthPart < 10 ? '0' + monthPart : monthPart
      datePart = datePart < 10 ? '0' + datePart : datePart

      return monthPart + '/' + datePart + '/' + yearPart + ' ' + hourPart + ':' + minutePart + ampm
    },

    filterDate: function (value) {
      let date = new Date(value)
      let monthPart = date.getMonth() + 1
      let datePart = date.getDate()
      let yearPart = date.getFullYear()

      return monthPart + '/' + datePart + '/' + yearPart
    },

    filterMoney: function (value) {
      return new Intl.NumberFormat().format(value)
    }
  },

  methods: {
    formatFullAddress: function (roomNo, houseNo, bldgName, purok, subdivisionName, barangay, city, province) {
      console.log('ahuh', province)
      roomNo = '' || roomNo + ' '
      houseNo = '' || houseNo + ' '
      bldgName = '' || bldgName + ' '
      purok = '' || purok + ' '
      subdivisionName = '' || subdivisionName + ' '
      barangay = barangay.BRGY_NAME === null ? '' : barangay.BRGY_NAME + ' '
      city = city.CITY_MUN_DESC === null ? '' : city.CITY_MUN_DESC + ' '
      province = province.PROVINCE_NAME === null ? '' : province.PROVINCE_NAME + ' '
      return roomNo + houseNo + bldgName + purok + subdivisionName + barangay + city + province
    }
  }
}
