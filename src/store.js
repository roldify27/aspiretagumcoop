import Vue from 'vue'
import Vuex from 'vuex'
import visitor from './store/visitor'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    visitor
  }
})
